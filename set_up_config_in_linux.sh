#!/bin/bash

# Folders
dotfiles_folder="$HOME/dotfiles"
home_folder="$HOME"

# The relative configuration file paths 
config_files=(
    ".tmux.conf"

    ".bashrc.d/alias"

    ".config/nvim/init.lua"
    ".config/nvim/coc-settings.json"
    ".config/nvim/lua/utils.lua"
)

function ensure_directory() {
    local file_path=("$@")
    # Ensure the file path contains a directory level 
    if [[ $file_path == *"/"* ]]; then
        # Get directory path of file
        directory_path=${file_path%/*}
        # Creates directory if it doesn't exist otherwise do nothing
        mkdir -p ${directory_path}
    fi
}

function hard_link_from_source_to_target() {
    local source_base_path=$1
    local target_base_path=$2

    # Using global variable config_files from above
    for file_path in "${config_files[@]}" ; do
        source_file_path="${source_base_path}/${file_path}"
        target_file_path="${target_base_path}/${file_path}"
        ensure_directory "$target_file_path"
        echo -e "hard link file from: '${source_file_path}' to '${target_file_path}'"
        ln -f $source_file_path $target_file_path
    done
    echo Success!
}

__describe_files="
The following files are tracked. 
They are defined at the beginning of the batch script
with relative paths.
"
echo "$__describe_files"
for file_path in "${config_files[@]}" ; do
    echo $file_path
done

__describe_options="
Hard link files from the source folder to the target folder.
If a file is already existing at the target location it will be overwritten.

Option [1] from '$dotfiles_folder' to '$home_folder'
Option [2] from '$home_folder' to '$dotfiles_folder' 
"
echo "$__describe_options" 

__choose_option="Please choose an option by writing 1 or 2. 
Writing anything else will abort the script. 
"
read -p "$__choose_option" option
case $option in  
  1) hard_link_from_source_to_target "$dotfiles_folder" "$home_folder" ;;
  2) hard_link_from_source_to_target "$home_folder" "$dotfiles_folder" ;;
  *) echo -e "Doing nothing" ;; 
esac

