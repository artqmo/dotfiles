# dotfiles

Configuration files for the programs I use.
Including neovim configured with plugins.

## Usage

Initially clone this repo into `~/dotfiles`.

On Linux move the content including the `.git` folder into `~/.config`.

On Windows move the content including the `.git` folder into `~/AppData/Local`.

Now you can delete the empty folder `~/dotfiles`.
With this setup you can track and update your config changes.
By default the `.gitignore` ignores everything except certain configuration
files.
To track configuration files these have to be explicitly added to the
`.gitignore`.
