-- General Neovim settings
local home_folder = require('utils').home_folder()
local vim = vim
local o = vim.opt
-- local autocmd = vim.api.nvim_create_autocmd
local g = vim.g
-- local cmd = vim.cmd

g.mapleader = ' '
g.maplocalleader = ' '

-- config netrw
g.netrw_keepdir = 0
g.netrw_localcopydircmd = 'cp -r'

o.wildmode = { 'longest' , 'list' , 'full' }
o.wildmenu = true
o.exrc = true

o.relativenumber = true
o.nu = true

o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
-- o.expandtab = true -- not using this for accessibility reasons
o.smartindent = true
o.wrap = false

o.hidden = true
o.dir = '/tmp'
o.swapfile = false
o.undodir = home_folder .. '/.share/nvim/undodir'
o.undofile = true
o.backup = false

o.smartcase = true
o.ignorecase = true
o.hlsearch = false
o.incsearch = true
-- o.syntax = 'yes'

o.scrolloff = 8
o.errorbells = false
o.signcolumn = 'yes'
o.updatetime = 50
o.colorcolumn = '80'
o.signcolumn = 'number'
o.termguicolors = true
o.splitright = true

-- set isfname+=@-@
o.shortmess = o.shortmess:append('c')


-- plugin section
-- first, define which plugins should be installed
require "paq" {
	"savq/paq-nvim"; -- let Paq manage itself

	-- theming
	'navarasu/onedark.nvim';
	"ellisonleao/gruvbox.nvim"; -- alternative gruvbox completly in lua
	"nvim-lualine/lualine.nvim";    -- statusline

	-- Telescope
	'nvim-lua/plenary.nvim';
	'nvim-telescope/telescope.nvim';
	'nvim-telescope/telescope-fzy-native.nvim';
	'nvim-telescope/telescope-dap.nvim';

	-- Treesitter
	{
		'nvim-treesitter/nvim-treesitter',
		build = function()
			pcall(require('nvim-treesitter.install').update { with_sync = true })
		end,
	};

	-- Additional text objects via treesitter
	{
    'nvim-treesitter/nvim-treesitter-textobjects',
    after = 'nvim-treesitter',
	};

	-- Comments
	'numToStr/Comment.nvim';

	-- LSP Support
	{ 'neoclide/coc.nvim', branch = 'release' };
	-- {'neovim/nvim-lspconfig'};

	-- Debug Support
	'mfussenegger/nvim-dap';
	'mfussenegger/nvim-dap-python';
	'theHamsta/nvim-dap-virtual-text';
	'rcarriga/nvim-dap-ui';
}

-- Install, Update and Clean Packages after initialization
-- require('paq'):sync() -- TODO: run silent? or in self closing window?

-- Set colorscheme
vim.o.background = "dark" -- or "light" for light mode
vim.o.termguicolors = true
-- require('onedark').setup {
--     style = 'warmer'
-- }
-- require('onedark').load()
require("gruvbox").setup({
  undercurl = true,
  underline = true,
  bold = true,
  -- italic = false,
  strikethrough = true,
  invert_selection = false,
  invert_signs = false,

  invert_tabline = false,
  invert_intend_guides = false,
  inverse = true, -- invert background for search, diffs, statuslines and errors
  contrast = "hard", -- can be "hard", "soft" or empty string
  palette_overrides = {},
  overrides = {},
  dim_inactive = false,
  transparent_mode = false,
})
vim.cmd("colorscheme gruvbox")
-- Set lualine as statusline
-- See `:help lualine.txt`
require('lualine').setup {
  options = {
    icons_enabled = false,
    -- theme = 'onedark',
    theme = 'gruvbox',
    component_separators = '|',
    section_separators = '',
  },
}

require('telescope').load_extension('fzy_native')
require('telescope').load_extension('dap')

-- gcc will toggle comment
require('Comment').setup()

require('nvim-treesitter.configs').setup {
	-- A list of parser names, or "all"
	ensure_installed = { "python", "lua", "javascript", "rust", "dockerfile",
		"html", "json", "latex", "markdown", "nix", "regex", "scss", "toml", "yaml"
	},

	-- Install parsers synchronously (only applied to `ensure_installed`)
	sync_install = false,

	-- List of parsers to ignore installing (for "all")
	ignore_install = {},

	highlight = {
		-- `false` will disable the whole extension
		enable = true,

		-- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
		-- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
		-- the name of the parser)
		-- list of language that will be disabled
		disable = {"html"},

		-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
		-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
		-- Using this option may slow down your editor, and you may see some duplicate highlights.
		-- Instead of true it can also be a list of languages
		additional_vim_regex_highlighting = false,
	},
}

--  'coc-jedi', 'coc-diagnostic', 
--  'coc-pyright'
vim.g.coc_global_extensions = { 'coc-json', 'coc-tsserver',
	'coc-json', 'coc-html', 'coc-css', 'coc-markdown-preview-enhanced',
	'coc-prettier', 'coc-rust-analyzer', 'coc-toml', 'coc-yaml',
	'coc-sumneko-lua', 'coc-pairs', 'coc-pyright', 'coc-diagnostic'
}

-- Configure debugger
-- require('dap-python').setup(home_folder .. '/.share/venv/bin/python')
-- require('dap-python').test_runner = 'pytest'
local dap = require('dap')

dap.adapters.python = {
	type = 'executable';
	command = vim.fn.getcwd() .. '/.venv/bin/python';
	args = { '-m', 'debugpy.adapter' };
}

dap.configurations.python = {
	{
		-- The first three options are required by nvim-dap
		type = 'python'; -- the type here established the link to the adapter definition: `dap.adapters.python`
		request = 'launch';
		name = "Launch file";
		program = "${file}";
	},
	{
		-- The first three options are required by nvim-dap
		type = 'python'; -- the type here established the link to the adapter definition: `dap.adapters.python`
		request = 'launch';
		name = "Launch file two";
		program = "${file}";
	}
}
-- local dap = require('dap')
-- table.insert(dap.configurations.python, {
-- 	-- The first three options are required by nvim-dap
-- 	type = 'python'; -- the type here established the link to the adapter definition: `dap.adapters.python`
-- 	request = 'launch';
-- 	name = "Python Flask";
--
-- 	-- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options
-- 	-- From the Flask specific JSON configuration in VSCode
-- 	module = "flask";
-- 	env = {
-- 		FLASK_APP = "app.py";
-- 		FLASK_ENV = "development";
-- 	};
-- 	args = { "run", "--no-debugger" };
-- 	jinja = true;
-- 	justMyCode = true;
-- 	pythonPath = function()
-- 		-- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
-- 		-- The code below looks for a `.venv` folder in the current directly and uses the python within.
-- 		-- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
-- 		local cwd = vim.fn.getcwd()
-- 		if vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
-- 			return cwd .. '/.venv/bin/python'
-- 		else
-- 			return '/usr/bin/python'
-- 		end
-- 	end;
-- })
--

-- MAPPINGS
-- local map = vim.keymap.set
-- local function map(mode, lhs, rhs_lua_function)
--     -- wraps the rhs_lua_function as recommended in the docs
--     opts = {silent = true}
--     vim.keymap.set(mode, lhs, function() return rhs_lua_function end, opts)
--     -- vim.keymap.set(mode, lhs, function() return rhs_lua_function() end, { silent = true })
-- end

-- local default_options = { silent = true }
-- local expr_options = { expr = true, silent = true }

-- Telescope
vim.keymap.set('n', '<C-p>', ':Telescope<CR>')
vim.keymap.set('n', '<leader>fe', ':Telescope extensions<CR>')
vim.keymap.set('n', '<leader>ff', ':Telescope find_files hidden=true<CR>')
vim.keymap.set('n', '<leader>fa', ':Telescope find_files hidden=false<CR>')
vim.keymap.set('n', '<leader>fh', ':Telescope help_tags<CR>')
vim.keymap.set('n', "<leader>fb", ':Telescope buffers<CR>')
vim.keymap.set('n', '<leader>fw', ':Telescope live_grep<CR>')

-- CoC, Conquer of Completion
vim.api.nvim_set_keymap("n", "<leader>ca", "<Plug>(coc-codeaction)", {})
vim.api.nvim_set_keymap("n", "<leader>caf", ":CocCommand eslint.executeAutofix<CR>", {})
vim.api.nvim_set_keymap("n", "gd", "<Plug>(coc-definition)", { silent = true })
vim.api.nvim_set_keymap("n", "K", ":call CocActionAsync('doHover')<CR>", { silent = true, noremap = true })
vim.api.nvim_set_keymap("n", "<leader>cr", "<Plug>(coc-rename)", {})
-- vim.api.nvim_set_keymap("n", "<leader>cf", ":call CocAction('format')<CR>", { silent = true, noremap = true })
vim.api.nvim_set_keymap("n", "<leader>cf", ":call CocAction('format')<CR>", {})
-- vim.api.nvim_set_keymap("n", "<leader>f", ":CocCommand prettier.formatFile<CR>", { noremap = true })
vim.api.nvim_set_keymap("i", "<C-Space>", "coc#refresh()", { silent = true, expr = true })
-- vim.api.nvim_set_keymap("i", "<TAB>", "pumvisible() ? '<C-n>' : '<TAB>'", { noremap = true, silent = true, expr = true })
-- vim.api.nvim_set_keymap("i", "<S-TAB>", "pumvisible() ? '<C-p>' : '<C-h>'", { noremap = true, expr = true })
-- select_suggestion="pumvisible() ? coc#_select_confirm() : '<C-G>u<CR><C-R>=coc#on_enter()<CR>'"
-- vim.api.nvim_set_keymap("i", "<CR>", select_suggestion, { silent = true, expr = true, noremap = true })
local select_suggestion="pumvisible() ? coc#_select_confirm() : '<TAB>'"
vim.api.nvim_set_keymap("i", "<TAB>", select_suggestion, { silent = true, expr = true, noremap = true })

-- Testing
vim.api.nvim_set_keymap("n", "<leader>pt", ":! python -m pytest --doctest-modules % <CR>", {})

-- Debugging, b for bugging
vim.api.nvim_set_keymap("n", "<leader>bb", ":lua require('dap').toggle_breakpoint() <CR>", {})
-- vim.api.nvim_set_keymap("n", "<leader>br", ":lua require('dap').repl.toggle(nil, 'vsplit') <CR>", {})
vim.api.nvim_set_keymap("n", "<A-h>", ":lua require('dap').continue()<CR>", {})
vim.api.nvim_set_keymap("n", "<A-k>", ":lua require('dap').step_out()<CR>", {})
vim.api.nvim_set_keymap("n", "<A-l>", ":lua require('dap').step_into()<CR>", {})
vim.api.nvim_set_keymap("n", "<A-j>", ":lua require('dap').step_over()<CR>", {})
vim.keymap.set('n', '<leader>bs', ':Telescope dap variables<CR>')
vim.keymap.set('n', '<leader>bf', 'Telescope dap frames<CR>')
vim.keymap.set('n', '<leader>bl', 'Telescope dap list_breakpoints<CR>')
vim.keymap.set('n', '<leader>bc', 'Telescope dap commands<CR>')

