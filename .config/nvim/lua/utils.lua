local M = {}

function M.home_folder()
    -- return the home folder of the user in Linux or Windows systems
    local unix_home = os.getenv('HOME')
    local windows_home = os.getenv('UserProfile')
    local home = nil

    if unix_home ~= nil then
        home = unix_home
    elseif windows_home ~= nil then
        home = windows_home
    else
        error('Could not read user home folder')
    end

    return home
end

return M

